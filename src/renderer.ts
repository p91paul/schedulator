/*
Copyright (C) 2018 Paolo Inaudi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
'use strict'

import * as $ from "jquery";
import * as Bootstrap from "bootstrap";
import "bootstrap-datepicker";

$('#startDate').datepicker({
  language: "it",
  daysOfWeekHighlighted: "0,6",
  todayHighlight: true
});
